# How To Install PostgreSQL on Ubuntu 20.04 [Quickstart]

```
sudo apt update
```
```
sudo apt install postgresql postgresql-contrib
```


check version
```shell
psql --version
```
get 

psql (PostgreSQL) 12.8 (Ubuntu 12.8-0ubuntu0.20.04.1)

https://www.digitalocean.com/community/tutorials/how-to-install-postgresql-on-ubuntu-20-04-quickstart